import yaml

def loadyaml(filename):
    with open(filename,'r',encoding='UTF-8') as f:
        data = yaml.load(stream=f,Loader=yaml.FullLoader)
        return data

