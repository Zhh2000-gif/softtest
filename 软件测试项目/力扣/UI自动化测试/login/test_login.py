import time

import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from config.loadlogin import loadyaml


class TestLogin:
    def setup(self):
        # 绕过js验证
        option = Options()
        option.add_experimental_option('excludeSwitches', ['enable-automation'])
        option.add_argument('--disable-blink-features=AutomationControlled')
        option.add_argument(r'--user-data-dir=C:\Users\Administrator\AppData\Local\Google\Chrome\User Data\Default')

        self.wb = webdriver.Chrome(options=option)
        self.wb.implicitly_wait(10)
    def teardown(self):
        self.wb.quit()
    @pytest.mark.parametrize('udata',loadyaml('../data/login.yaml'))
    def test_login(self,udata):
        self.wb.get('https://leetcode.cn/')
        self.wb.find_element('xpath','//*[@data-cypress="sign-in-with-password"]').click()
        self.wb.find_element('xpath','//input[@name="login"]').send_keys(udata['uname'])
        self.wb.find_element('xpath','//input[@name="password"]').send_keys(udata['upwd'])
        self.wb.find_element('xpath','//span[@role="checkbox"]').click()
        self.wb.find_element('xpath','//button[@type="submit"]').click()
        time.sleep(3)
        self.wb.find_element('xpath','//a[text()="学习"]').click()
        time.sleep(2)
        self.wb.find_element('xpath','//div[@class="css-1eb1aaw-dropdown-card-Subject elj7k3h1"]').click()
        self.wb.find_element('xpath','//div/a/h1[text()="硬核操作系统指南"]').click()
        time.sleep(1)
        self.wb.find_element('xpath','//div/button/span[text()="收藏"]').click()
        self.wb.find_element('xpath','//span[@class="css-17dlube-AvatarWrapper e1ajzf6f1"]').click()
        self.wb.find_element('xpath','//a[text()="收藏夹"]').click()
        self.wb.find_element('xpath','//*[@id="lc-content"]/div/div[2]/div[1]/a[2]').click()
        ele = self.wb.find_element('xpath','//*[@id="lc-content"]/div/div[2]/div[2]/a/div[2]/div[1]/div[1]/h1')
        assert ele.text=='硬核操作系统指南'


