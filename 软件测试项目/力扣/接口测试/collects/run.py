import os

import pytest

if __name__ == '__main__':
    pytest.main(['-vs','--alluredir','../allure-result'])
    os.system('allure generate ../allure-result -o ../reports')