

1.确认adb的版本一致，Android SDKK 和模拟器（adb.exe/nox_adb.exe）

2.连接手机

​	adb connect 127.0.0.1:62001[夜神模拟器62001 mumu模拟器7555]

3.打开Appium 中的Edit Configuration 配置Andorid Home  Java Home

4.点击appium主页start Server按钮

5.填写appium server信息

​	Remote Host 127.0.0.1

 	Remote Port 4723

​	Remote Path /wd/hub wd--》webdriver  hub--》appium

6.配置Capabilities，以雪球为例

​	

adb常用命令

![adb常用命令](D:\PythonProjects\gitProjects\softtest\软件测试\App自动化测试\adb常用命令.png)

打开app查看最近活动

adb shell dumpsys activity recents | find "intent={"

cmp=tv.danmaku.bili/.ui.splash.SplashActivity 包名/活动名

查看当前界面包名和活动页名称

adb shell dumpsys window windows | findstr mFocusedApp

查看启动的app包名

adb shell dumpsys activity top | find "ACTIVITY"

查看所有启动的APP 包名

adb shell dumpsys activity activities | findstr "Run"

### ADB命令（Android Debug Bridge）

```
adb构成
	client端，在电脑上，负责发送adb命令
	daemon守护进程，在手机上，负责接受和执行adb命令
	server端，在电脑上，负责管理client和daemon之间的通信
adb工作原理
	client端发送命令给server端
	server端会将命令发送给daemon端
	daemon端进行执行
	将执行结果，返回给server端
	server端将结果再返回给client端
	
adb获取包名和界面名
	包名：决定程序的唯一性（不是应用的名字）
	界面名：一个界面名，对应一个界面
	
adb文件传输
	传文件到手机 adb push 电脑文件路径  手机文件路径
	从手机拉取文件 adb pull 手机路径 电脑文件路径
	
adb 获取app启动时间
	adb shell am start -w 包名/启动名
	ThisTime：该界面启动耗时
	TotalTime：应用自身启动耗时
	WatiTime：系统 启动应用耗时
	
adb获取手机日志
	1.打开测试的应用程序
	2.找到触发bug的位置
	3.使用查看日志命令
	4.触发bug
	5.获取日志信息
	adb logcat  
```



### Appium

原理图

![image-20221116143344432](D:\PythonProjects\gitProjects\softtest\软件测试\App自动化测试\Appium原理图.png)

安装client客户端库，用于客户端和Appium Server进行通信

```
pip install appium-python-client
```

```
通过脚本进行自动化测试
from Appium import webdriver
driver = webdriver.Remote("http://localhost:4723/wd/hub",desired_caps)

在脚本中启动其他app
driver.start_activity(appPackage,appActivity)
获取包名
driver.current_package
获取界面名
driver.current_activity
```

#### 将应用置于后台

```
app在进入后台一段时间后，再回到前台也会重新输入密码
deiver.background_app(seconds)
```

#### 滑动和拖拽事件

```
1.1 swipe滑动屏幕
	driver.swipe(start_x,start_y,end_x,end_y,duration=None)
	滑动有惯性，持续时间越长，惯性越小
1.2 scroll滑动屏幕
	driver.scroll(origin_el,destination_el)
	origin_el 滑动开始的元素
	destination_el 滑动结束的元素
	惯性很大
1.3 drag_and_drop滑动屏幕
	driver.drag_and_drop(origin_el,destination_el)
	origin_el 滑动开始的元素
	destination_el 滑动结束的元素
	没有惯性
```

#### 高级手势TouchAction

```
TouchAction可以实现一些争对手势的操作，比如滑动、长按、拖动等，我们可以将这些基本手势组成一个相对复杂的手势
1.1 手指轻敲操作
	TouchAction(driver).tap(element=None,x=None,y=None).perform()
1.2 按下和抬起操作
	按下 TouchAction(driver).press(el=None,x=None,y=None).perform()
	抬起 TouchAction(driver).release().perform()
1.3 等待操作
	TouchAction(driver).wait(ms=0).perform()
1.4 长按操作
	TouchAction(driver).long_press(el=None,x=None,y=None,duration=1000).perform()
1.5 移动操作
	TouchAction(driver).move_to(x=None,y=None)
```

#### 分辨率和截图

```
1.1 获取手机分辨率
driver.get_window_size()["width"]
1.2 截图
driver.get_screenshot_as_file("screen.png")
1.3 获取手机网络
driver.set_network_connection
	0、无网络
	1.飞行模式
	2.仅wifi
	4.仅数据网络
	6.所有的网络
```

#### 发送键到设备

```
模拟手机的按键发送，例如50代表home键
driver.press_keycode(keycode,metastate=None)
```

#### 操作通知栏

```
driver.open_notifications()
	
修改位置
	driver.set_location(longitude=,latitude=,speed=)
	driver.find_element().is_displayed() 可见
	driver.find_element().is_enabled() 编辑
	driver.find_element().is_selected() 选中
```



#### 界面查看工具

uiautomator  在AndroidSDK下的tools下，uiautomator.bat

Appium Inspector

定位元素方法

from appium.webdriver.common.appiumby import AppiumBy

id  driver.find_element_by_id

class_name 

 accessibility id

Xpath

通过UiSelector这个类里面的方法实现元素定位

```
code = 'new UiSelector().text("热门").className("android.widget.TextView")' driver.find_element(AppiumBy.ANDROID_UIAUTOMATOR, code) 

ele.click()
1.id方法
	resourceid
	resourceIdMatches()正则表达式

2.text 方法
可以根据元素的文本属性查找元素
textContains
根据文本包含什么字符串
textStartsWith
根据文本以什么字符串开头
textmartch 方法
可以使用正则表达式 选择一些元素
3.description描述方法
	description
	descriptionContains
	descriptionStartWith
	descriptionMatches
4.class 方法
	className("")
5.索引、实例方法
	index() 编号
	instance("") 索引
6.特殊属性
	checked()
	checkable()
7.多属性组合
	new UiSelector.resourceId("").text("")
driver.find_element(MobileBy.ANDROID_UIAUTOMATOR,'new UiSelector.resourceId("").text("")')
```

#### Monkey

```
Monkey 是一个在模拟器或设备上运行的程序，可生成伪随机用户事件（例如点击、轻触或手势）流以及很多系统级事件。您可以使用 Monkey 以随机且可重复的方式对正在开发的应用进行压力测试。

记录崩溃率和卡顿比

常见的检测崩溃的手段
	业务测试
	环境版本兼容测试
	健壮性测试（数据量大）
	monkey测试
	友盟等日志统计
崩溃等级    
    严重 千分之三  ios千分之十五
    轻微 千分之四到二之间 千分之八到二
    标准 二到四	 三到八
    优秀 0~2      0~3
    
用真机时加-p命令***
	
```

