appium.webdriver.extensions.android package
===========================================

Submodules
----------

appium.webdriver.extensions.android.activities module
-----------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.activities
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.common module
-------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.common
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.display module
--------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.display
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.gsm module
----------------------------------------------

.. automodule:: appium.webdriver.extensions.android.gsm
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.nativekey module
----------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.nativekey
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.network module
--------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.network
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.performance module
------------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.performance
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.power module
------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.power
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.sms module
----------------------------------------------

.. automodule:: appium.webdriver.extensions.android.sms
   :members:
   :undoc-members:
   :show-inheritance:

appium.webdriver.extensions.android.system\_bars module
-------------------------------------------------------

.. automodule:: appium.webdriver.extensions.android.system_bars
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: appium.webdriver.extensions.android
   :members:
   :undoc-members:
   :show-inheritance:
