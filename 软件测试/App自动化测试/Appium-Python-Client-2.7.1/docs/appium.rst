appium package
==============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   appium.common
   appium.webdriver

Submodules
----------

appium.version module
---------------------

.. automodule:: appium.version
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: appium
   :members:
   :undoc-members:
   :show-inheritance:
