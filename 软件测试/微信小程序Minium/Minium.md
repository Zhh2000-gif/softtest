### Minium

```
一、信小程序的自动化测试方案
1.利用屏幕上xy坐标，模拟用户的交互
2.利用devtools，调试webview内容
3.利用框架，进行深入复杂自动化测试

二、搭建环境
安装minium
配置工具路径
执行minitest
编写自动化测试用例的代码

三、配置工具路径
//config.json
{
	"project_path":"项目路径"
	"dev_tool_path":"微信开发者工具.bat"
}

四、编写自动化测试用例的代码
minium是基于unittest进行封装
class Mytest(minium.MiniTest)
print(self.mini.get_system_info())
```



#### 核心组件

```
self.mini:小程序driver，连接开发者工具
self.app：小程序
self.page：当前的页面
self.native：微信的原生控件
```

#### 知识点

```
1，某些元素是不可被定位到的
2.小程序中页面，分为普通页面、tabBar页面
```

#### 元素定位及自动等待

```
url=""
self.app.relaunch('url')

ele=self.page.get_element(xpath路径)定位单个元素
self.page.get_elements()定位多个元素

ele.input()
hook技术
ele.trigger()监听操作然后做下一步

ele.click()
 
让程序进行等待，使用最大的等待时间max_timeout=
定位元素复制路径不行，可以使用标签名.属性名
ele = self.page.get_element(
	'view',
	text_contains="",
	max_timeout=100
)
self.page.waiting_for()等待元素出现

```



#### 页面的类型和跳转方法



#### 数据驱动测试执行多个用例



#### 生成HTML+截图的测试报告

