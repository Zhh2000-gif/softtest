HttpRunner：

##### 什么是HttpRunner

​	面向http协议的框架，只需要去维护一份yaml/json文件就可以使用自动化测试，结合locust性能测试，线上性能监控，持续集成等多种需求。devops，requests

##### 设计思想和理念

1.重复复用优秀的开源项目

2.约定大于配置

3.配置文件组织测试用例（yaml/json）

4.一次投入，多处复用、

5.高度的可扩展性