### Python测试接口：

requests：

​	get

​	post

​	delete

​	put

​	request()；

数据：

​	data：dict字典类型  str类型

​			json.dump()将str 转化为 dict

​	json：json字符串

### Pytest

​	1.main方法启动

​		-v

​		-s

​		-n  多线程运行

​		--reruns=2  重新跑失败用例2次

​		--html 生成测试报告

​	2.pytest.ini配置文件

#### 前后置、夹具

setup/teardown 在每个用例之前/之后执行

setup_class/teardown_class 在每个类之前/之后执行

单个用例执行：

​	@pytest.mark.xx

部分前置：

​	可以通过yield唤醒类似于teardown的功能

​	@pytest.fixture(scope 作用域，autouse  是否自动执行)一般情况下和conftest.py文件一起使用

​    conftest.py中的@pytest.fixture()可以直接调用。

#### 接口关联的封装

​	创建yaml文件

​	读取yml文件方法

​	写入yml文件方法

​		获取类变量，从而不用在每个类里都去定义。

​	例如获取token，每次获取不同的值会再yml文件中重复，所以采用conftest定义一个自动执行的方法。

import yaml;

读取yml文件：with open(os.getcwd+"/xx.yml",mode="r",encoding="UTF-8") as f:

​								value = yaml.load(stream=f,Loader=yaml.FullLoader);

​								retuen value;

导入yml文件：with open(os.getcwd+"/xx.yml",mode="r",encoding="UTF-8") as f:

​								yaml.dump(data=data,stream=f,allow_unicode=True)

读取测试用例的yml文件

​	def read_testcase_yaml(self,yaml_name):

​		with open(os.getcwd() + "/testcases/"+yaml_name,mode="r",encoding="UTF-8") as f:

​			value = yaml.load(stream=f,Loader=yaml.FullLoader)

​			retuen value

#### pytest接口断言

​	assert 进行判断

​	assert 'url' in rep.json() and 1==1

#### pytest结合allure-pytest生成allure测试报告

​	(1)生成了临时的json文件

​		启动方法中的参数加上 --alluredir ./temp

​	(2)通过临时的json文件生成allure报告

​	os.system("allure generate temp -o reports --clean")

### YAML数据驱动的封装

@pytest.mark.parametrize(args_name 参数名,args_value 参数值)

参数值： list  tuple元组  字典列表  字典元组  数据有多少个值，就会执行多少次

```
class TestApi:
    # @pytest.mark.parametrize('args',['a','b','c'])
    # def test_api1(self,args):
    #     print("章恒浩")

    @pytest.mark.parametrize('name,age',[{'a',12},{'b',18},{'c',20}])
    def test_api2(self,name,age):
        print(name,age)
if __name__ == '__main__':
    pytest.main(['-vs','test_api.py'])
```

@pytest.mark.parametrize('caseinfo',YamlUtil().read_testcase_yaml('get_token'))

caseinfo ['name']['method']

###### yaml详解：

主要作用

​	1.配置文件

​	2.测试用例

数据组成

​	map对象：键 空格 值对

​						列表  用-开头的

```
maxy
  -name：百里
  -sex：男
YAML格式用例的约定：
	1.必须包含一级关键字：name,request,validate
	2.在request关键字下必须包括：method，url,data
	3.提取变量使用一级关键字extract 支持json提取和正则提取（.+?）和(.*?),取值用{{}}
	4.可以使用热加载的方式调用debug_talk.py中DebugTalk类里面的方法,通过${}调用方法
	5.支持equals,contains两种断言
    6.使用parameters做csv文件的数据驱动，通过$csv{appid}这种格式取值
```

##### 一、pytest中装饰器@pytest.mark.parametrize()的基础用法

数据驱动：其实就是把测试用例的数据放到excel、yaml、csv、mysql，然后通过去改变数据达到改变测试用例的执行结果。

​	值得格式：[],{},[{},{}],{{},{}}.几个参数执行几次

#### unittest单元测试框架

单元测试:测试程序的最小单元

多接口关联:

​	yaml文件中保存多个值怎么办，会有什么问题？值会一直增加

​	unittest的前后置

#### unittest框架主要做了什么

1.测试发现

2.测试执行

3.测试判断

4.测试报告

#### unittest重要组件

1.TestCase 测试用例

2.TestSuite 测试套件

3.TestFixture 测试脚手架

4.TestLoader

##### 5.TestRunner 测试运行器

##### TestCase用法

导入

（unittest.TestCase）

执行结果：

​	Pass 成功 F 失败  E  错误  S  跳过的标记为成功

用例执行的顺序

​	ASCALL属性 a-z A-Z 0-9

匹配模式

​	1.通配符：-k*_weiwei

​	2.字符串：-l weiwei

##### 框架底层原理：

​	main方法，关联的类包括的一些参数，main（）代表初始化对象，运行init方法。

​	unittest.defaultTestLoader.discover(),用默认的加载器加载对应的用例

#### 断言

​	assertEqual  assertTrue assertIn

#### 接口自动化之Cookie，Session，Token鉴权解决方案

​	Http协议之间的请求是无连接 无状态，请求之间没有关联的，为了i请求之间的信息共享

一、Cookie

​	Cookie产生在客户端，每次请求服务端的时候都会去验证Cookie

​	Cookie鉴权方式：通过第一次请求获取cookie作为全局变量，再放到其他请求中。

二、session

​	类似于cookie，但是存储在服务器端

三、token

​	可以通过抓包进行抓取，加密。用户的令牌，下次登录需要带上

#### 自动化测试框架

POM(TestCase测试用例层)

封装的方法层（数据驱动、全局ini配置文件）

excel/csv/mysql数据库

HTML报告、日志文件

#### 自动化测试框架执行

1.通过Testsuit

suit=unittest.Testsuit()

suit.addTest()

runner=unittest.HTMLTestRunner()

runner.run(suit)

2.通过TestLoader

suite1=unittest.TestLoader().loadTestFromTestCase(Testcase1)

suite=unittest.TestSuit(suite1)

unittest.HTMLTestRunner(verbosity=2).run(suite)

3.通过defaultTestLoader

discover = unittest.defaultTestLoader.discover(test_module, pattern="test*.py")