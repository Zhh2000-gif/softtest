```
绕过js验证
option = Options()
option.add_experimental_option('excludeSwitches',['enable-automation'])
option.add_argument('--disable-blink-features=AutomationControlled')
绕过IP验证
option.add_argument(r'--user-data-dir='浏览器用户数据位置')
```



## Selenium

```
自动化的完成 人的一些操作，例如点击、输入等操作，也可以获取信息，然后程序进行分析处理
```

```---
架构图：
	自动化程序（selenium 客户端库）《----》浏览器驱动《----》浏览器
```

##### 环境准备

```
chrome浏览器  chrome驱动程序  pycharm
```

##### 简单示例

```
from selenium import webdriver
from selenium.webdriver.chrome.service import Service

wb = webdriver.Chrome(service=Service(r'驱动路径'))
wb.get('http://www.baidu.com')

input()
```

##### 页面根据id获取元素

```
from selenium import webdriver
from selenium.webdriver.common.by import By

wb = webdriver.Chrome(service=Service(r'驱动路径'))
wb.get('http://www.baidu.com')

element = wb.find_element(By.ID,'kw')

element.send_keys('通讯\n')

pass
```

##### 页面获取class、tag元素

```
wd.find_element_by_class_name();
返回列表，for循环获取元素的text内容.
find_element和find_elements区别：
	find_element是找到的元素中选择第一个
	find_elements是找到所有符合的元素
find_elements_by_tag_name('div')

Selenium4之后的写法
from selenium.webdriver.common.by import By

wd.find_element(By.ID)
wd.find_element(By.CLASS_NAME)
```

#### 通过Webelement对象

```
Webdriver对象是 整个web页面
Wevelement对象是 element元素的内部
```

#### 获取页面的结果

```
print(element.text())

执行点击动作后，涉及到后台的服务器，所以会有一定的等待时间，脚本执行速度快，但是没有获取到数据，可能会报错
可以通过time包中的sleep()时间来等待一会

通过异常处理的方式：
while True:
	try:
		element = wd.find_element(By.ID,'1')
		print(element.text)
		break
	except:
		time.sleep(1)

通过selenium的方法 implicitly_wait  隐式等待，设置最大的时间，后面的find_element和find_elements都会这样。
wd.implicitly_wait(10)

```

#### 操控元素的基本方法

```
1.点击 send_keys() 之后清除输入框的内容  clear()
2.element.get_attribute()
3.css选择器
	wd.find_element_by_css_selector('div')
	wd.find_element_by_css_selector('#id')
	
	元素2是元素1的直接子元素。
	元素1 > 元素2 
	
	元素2是元素1的后代元素
	元素1  元素2
	
	父元素的第n个子节点
	使用nth-child()
	例如 span:nth-child(2)
	倒数第n个节点
	nth-last-child(n)
	
	第n个某类型得子节点
	nth-of-type(n)
	
	某类型偶数节点 nth-of-type(even)
	某类型奇数节点 nth-of-type(odd)
	
	兄弟节点
	相邻兄弟节点 使用+  例如 h3+span
	后续所有兄弟节点的选择 ~ h3~span
	
```

#### frame切换/窗口切换

```
wd.switch_to.frame
wd.switch_to.window
```

#### 选择框

```
radio框 input
	获取当前选中的 element = wd.find_element_by_css_selector('#s_radio input[checked=checked]')
	
	点选 
	wd.find_element_by_css_selector('#s_radio input[value="小雷老师"]').click()
checkbox框 input
	要选中checkbox的一个选项，必须先获取当前复选框的状态，如果该选项已经勾选了，就不能再点击
	思路：
		先把已经选中的都点击一下，确保都是未选状态
		再进行点击
select框 option
	提供了一个select类 from selenium.webdriver.support.ui import Select
	先获取select对象
	select_all
	select_by_value
	select_by_index
	select_by_visible_text
	deselect_by_value 是去除 选中元素
```

#### 其他操控元素的方法

```
例如 鼠标右键点击、双击 移动鼠标到某个元素，鼠标拖曳等
可以通过Selenium提供的ActionChains类来实现

例如移动鼠标到某个元素
	ac = ActionChains(wd)
	ac.move_to_element(
		wd.find_element_by_css_selector('[name="tj_briicon"]')
	).perform()

```

#### 冻结界面的方法