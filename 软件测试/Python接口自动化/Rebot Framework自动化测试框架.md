### Rebot Framework自动化测试框架

1.创建项目，dictory

2.创建测试套件，File

​	关注：Edit页签，主要有四大块

​	Setting设置；

​		测试套件之前的准备工作

​		测试套件之前的扫尾工作

​		测试用例之前的准备工作

​		测试用例之前的扫尾工作

​		测试用例模板

​		测试用例超时的时间

​		强制标记：smoke 冒烟用例

​		默认标记

​		

​	Import：导入外部文件

​		Library：红色表示导入失败

​		关键字：蓝色成功，黑色失败

​	定义内部变量；

​	元数据：



3.创建测试用例

4.创建业务关键字 txt

5.创建用户自定义关键字 

### RF常用类库

1.标准库

​	Buitini 测试库

​	Collections集合库

​	DateTime时间库

​	ScreenShot截屏库

2.扩展库 需要pip安装

​	Web自动化测试：SeleniumLibrary，Selenium2Library,Selenium2Library for java

​	API接口自动化：RequestLibrary

​	APP自动化：AppiumLibrary

​	安装方式：pip install robotframework.seleniumLibrary

#### RF常用关键字的使用

快捷键：

1.搜索的快捷键：F5

2.自动补全关键字：ctrl+shift+空格



1.打印

Log 

2.设置变量

${a} Set Variable  100

3.获得系统时间

Get Time

4.睡眠时间，强制等待

sleep  3

5.字符串的拼接

${str}  Catnate  str1 str2

6.创建列表

${list}  Create List

用Log Many来保存@{list} 打印出来

7.创建字典

Create Dictionary

8.执行Python里面的方法

Evaluate

9.流程控制IF

${age}                  Set Variable    22

Run Keyword If  ${age}>30       Log      年龄太大

...                            ElSE IF            Log      abc

10.流程控制FOR

FOR   ${a}   IN   oracle   mysql

Log    ${a}

#### UI自动化测试

一、准备环境

1.pip install robotframework-seleniumlibrary

2.下载谷歌浏览器

3.下载谷歌浏览器的驱动，版本必须兼容，放到python包下

4.在RF测试套件中导入SeleniumLibrary

###### 二、通过关键字进行UI自动化测试的过程

Open Browser  https://www.baidu.com

Set Browser  5

Comment   Input Text    id=kw   章恒浩

Comment   Click  Element   link=新闻

###### 元素定位

前提：元素必须唯一



xpath：

​	1.通过绝对路径

​	2.通过相对路径：//form/span/input

​		Input Text xpath=//form/span/input  码尚学员

​	3.通过元素属性定位：//input[@autocomplate="off" and class=""]

​	4.通过部分属性定位

​	5.通过文本定位