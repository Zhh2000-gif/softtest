# -*- encoding：utf-8 -*-
# @Time:2023/1/3 09:30
# @Auto:guohuashang
# @FileName:FirstPage_SeaarchTestCase.py
# @Emial:
# @Function:
import unittest
from common.ElementAction import ElementAction
from common.BrowserOption import BrowserOptions
from common.readExcel import readExcel
class SearchTestCase(unittest.TestCase):
    def setUp(self) -> None:
        url = "https://www.baidu.com/"
        BrowserOptions().Browser_Url(url)
    def tearDown(self) -> None:
        pass
    def test_search001(self):
        excel=readExcel()
        method=excel.analysisExcel("firstpage",1,1)
        elementValue=excel.analysisExcel("firstpage",1,2)
        fileName=excel.analysisExcel("firstpage",1,4)
        ElementAction().Element_click(method,elementValue,fileName)
        ElementAction().Element_sendKeys(method,elementValue,fileName,"河南立哲科技")
    def test_search002(self):
        print("12345")
    def test_search003(self):
        print("test_search003")
# if __name__ == '__main__':
#     unittest.main()
