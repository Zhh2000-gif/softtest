# -*- encoding：utf-8 -*-
# @Time:2023/1/3 09:29
# @Auto:guohuashang
# @FileName:tiebaTestCase.py
# @Emial:
# @Function:

import unittest
from selenium import webdriver
from common.BrowserOption import BrowserOptions

class tiebaaTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        pass

    def setUp(self) -> None:
        BrowserOptions().Browser_Url("https://tieba.baidu.com/index.html")

    @classmethod
    def tearDownClass(cls) -> None:
        BrowserOptions().Browser_quit()

    def tearDown(self) -> None:
        pass

    def test_tieba001(self):
        print("test_tieba001")

    def test_tieba002(self):
        print("test_tieba002")


if __name__ == '__main__':
    unittest.main()