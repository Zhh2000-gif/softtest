# -*- encoding：utf-8 -*-
# @Time:2023/1/3 10:07
# @Auto:guohuashang
# @FileName:MouseOptions.py
# @Emial:
# @Function:
from selenium.webdriver.common.action_chains import ActionChains
from common.findElement import findElement
from common.BrowserOption import BrowserOptions
class MouseOptions:
    def __init__(self):
        self.driver=BrowserOptions().dr
    def MoveToElement(self,method,elementValue,fileName):
        element=findElement().Element_Method(method,elementValue,fileName)
        ActionChains(self.driver).move_to_element(element).perform()