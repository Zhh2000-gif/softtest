# -*- encoding：utf-8 -*-
# @Time:2023/1/3 11:57
# @Auto:guohuashang
# @FileName:singleton.py
# @Emial:
# @Function:
def singleton(cls,*args,**kw):
    instance={}
    def _singleton():
        if cls not in instance:
            instance[cls]=cls(*args,**kw)
        return instance[cls]
    return _singleton