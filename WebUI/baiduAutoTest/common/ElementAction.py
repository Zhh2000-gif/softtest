# -*- encoding：utf-8 -*-
# @Time:2023/1/3 11:24
# @Auto:guohuashang
# @FileName:ElementAction.py
# @Emial:
# @Function:
from common.findElement import findElement
class ElementAction:
    def Element_click(self,method,elementValue,fileName,index=None):
        if index==None:
            findElement().Element_Method(method,elementValue,fileName,index=None).click()
        else:
            findElement().Elements_Method(method,elementValue,fileName,index).click()
    def Element_cleaar(self,method,elementValue,fileName,index=None):
        if index == None:
            findElement().Element_Method(method, elementValue, fileName).clear()
        else:
            findElement().Elements_Method(method, elementValue, fileName, index).clear()
    def Element_sendKeys(self,method,elementValue,fileName,revText,index=None):
        if index == None:
            findElement().Element_Method(method, elementValue, fileName).send_keys(revText)
        else:
            findElement().Elements_Method(method, elementValue, fileName, index).send_keys(revText)