# -*- encoding：utf-8 -*-
# @Time:2023/1/3 14:15
# @Auto:guohuashang
# @FileName:readExcel.py
# @Emial:
# @Function:
import xlrd
class readExcel():
    def analysisExcel(self,sheetname,x,y):
        filepath='../datapool/element.xls'
        sheet=xlrd.open_workbook(filepath).sheet_by_name(sheetname)
        return sheet.cell_value(x,y)