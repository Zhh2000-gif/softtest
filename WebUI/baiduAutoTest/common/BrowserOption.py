# -*- encoding：utf-8 -*-
# @Time:2023/1/3 09:39
# @Auto:guohuashang
# @FileName:BrowserOption.py
# @Emial:
# @Function:
from common.singleton import singleton
from selenium import webdriver
@singleton
class BrowserOptions:
    def __init__(self):
        self.dr=webdriver.Firefox()
        self.dr.implicitly_wait(10)
    def Browser_Max(self):
        self.dr.maximize_window()
    def Browser_Mini(self):
        self.dr.minimize_window()
    def Browser_forward(self):
        self.dr.forward()
    def Browser_back(self):
        self.dr.back()
    def Browser_refresh(self):
        self.dr.refresh()
    def Browser_close(self):
        self.dr.close()
    def Browser_quit(self):
        self.dr.quit()
    def Browser_Url(self,url):
        self.dr.get(url)
