# -*- encoding：utf-8 -*-
# @Time:2023/1/3 09:49
# @Auto:guohuashang
# @FileName:findElement.py
# @Emial:
# @Function:
from selenium.webdriver.common.by import By
from common.BrowserOption import BrowserOptions
import logging
class findElement:
    def __init__(self):
        self.driver=BrowserOptions().dr

    # def findMethod(self,method,elementValue,index=None):
    #     if method.lower()=='id':
    #         if index is None:
    #             element=self.driver.find_element(By.ID,elementValue)
    #         else:
    #             element=self.driver.find_elements(By.ID,elementValue)[index]
    #     elif method.lower()=="class":
    #         pass
    #
    # def find_id(self,elementValue,fileName,index=None):
    #     try:
    #         if index is None:
    #             element=self.driver.find_element(By.ID,elementValue)
    #         else:
    #             element=self.driver.find_elements(By.ID, elementValue)[index]
    #     except Exception as err:
    #         self.driver.save_screenshot("../testReport/IMG/"+fileName+".png")
    #         logging.info("这是一个日志")
    #     else:
    #         return element
    # def find_xpath(self):
    #     try:
    #         elemnet=self.driver.find_element(By.XPATH,elementValue)
    #     except Exception as a:
    #         self.driver.save_screenshot("../testReport/IMG/" + fileName + ".png")
    #         logging.info("这是一个日志")
    #     else:
    #         return elemnet
    def Element_Method(self,method,elementValue,fileName,index=None):
        try:
            if method.lower()=="id":
                return self.driver.find_element(By.ID,elementValue)
            elif method.upper()=="CLASS":
                return self.driver.find_element(By.CLASS_NAME,elementValue)
            elif method.lower()=="link":
                return self.driver.find_element(By.LINK_TEXT,elementValue)
            elif method.lower()=="xpath":
                return self.driver.find_element(By.XPATH,elementValue)

        except Exception as e:
            self.driver.save_screenshot(f"../testReport/IMG/{fileName}.png")
    def Elements_Method(self,method,elementValue,fileName,index):
        try:
            if method.lower() == id:
                return self.driver.find_element(By.ID, elementValue)[index]
            elif method.upper() == CLASS:
                return self.driver.find_element(By.CLASS_NAME, elementValue)[index]
            elif method.lower() == link:
                return self.driver.find_element(By.LINK_TEXT, elementValue)[index]


        except Exception as e:
            self.driver.save_screenshot(f"../testReport/IMG/{fileName}.png")
