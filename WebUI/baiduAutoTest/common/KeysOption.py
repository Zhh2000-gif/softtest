# -*- encoding：utf-8 -*-
# @Time:2023/1/3 11:34
# @Auto:guohuashang
# @FileName:KeysOption.py
# @Emial:
# @Function:
from selenium.webdriver.common.keys import Keys
from common.BrowserOption import BrowserOptions

from common.ElementAction import ElementAction
class KeyOption:
    def __init__(self):
        self.browser=BrowserOptions().dr
    def keys_Enter(self,method,elementValue,fileName):
        ElementAction().Element_sendKeys(method,elementValue,fileName,Keys.ENTER)

    def niantie(self,method,elementValue,fileName):
        ElementAction().Element_sendKeys(method,elementValue,fileName,Keys.CONTROL,'v')