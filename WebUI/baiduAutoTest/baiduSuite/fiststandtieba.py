# -*- encoding：utf-8 -*-
# @Time:2023/1/3 14:30
# @Auto:guohuashang
# @FileName:fiststandtieba.py
# @Emial:
# @Function:
import unittest
from baiduTestCase import FirstPage_SeaarchTestCase
from baiduTestCase import tiebaTestCase
def moreSuite():
    more=unittest.TestSuite()
    more.addTest(unittest.makeSuite(FirstPage_SeaarchTestCase.SearchTestCase))
    more.addTest(unittest.makeSuite(tiebaTestCase.tiebaaTestCase))
    unittest.TextTestRunner().run(more)
moreSuite()