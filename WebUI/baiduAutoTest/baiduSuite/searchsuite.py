# -*- encoding：utf-8 -*-
# @Time:2023/1/3 14:25
# @Auto:guohuashang
# @FileName:searchsuite.py
# @Emial:
# @Function:
import unittest
from baiduTestCase.FirstPage_SeaarchTestCase import SearchTestCase
def searchSuite():
    search=unittest.TestSuite()
    searchlist=['test_search001','test_search003']
    for i in searchlist:
        search.addTest(SearchTestCase(i))
    # search.addTest(SearchTestCase('test_search003'))
    # search.addTest(SearchTestCase('test_search001'))
    unittest.TextTestRunner().run(search)
searchSuite()