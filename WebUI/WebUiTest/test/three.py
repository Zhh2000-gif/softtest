from time import sleep

from selenium import webdriver

from test import config
from selenium.webdriver.common.by import By
wb = webdriver.Chrome()
wb.get(config.url)
ele = wb.find_element(By.ID,"3")
ele.click()

act = wb.switch_to.alert
sleep(3)
ele = wb.find_element(By.CSS_SELECTOR,"#textSpan>font")
if ele.text=="您为何如此谦虚？":
    print("用户选择的是取消，内容为%s"%ele.text)
else:
    print("用户选择的是确定，内容为%s"%ele.text)