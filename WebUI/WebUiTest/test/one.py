from time import sleep

from selenium import webdriver

from test import config
from selenium.webdriver.common.by import By
wb = webdriver.Chrome()
wb.get(config.url)
ele = wb.find_element(By.ID,"1")
ele.click()

act = wb.switch_to.alert
sleep(5)
ele = wb.find_element(By.CSS_SELECTOR, "#textSpan>font")
if ele.text=="您没有按要求输入，请重新输入":
    print("用户的选择是取消")
else:
    print("用户选择的是确定，值为%s"%ele.text)

# if act.dismiss():
#     print("用户选择的是取消")
# elif act.accept():
#     ele = wb.find_element(By.CSS_SELECTOR,"#textSpan>font")
#     print("用户选择的是确定：值为%s"%ele.text)

