from time import sleep

from selenium.webdriver.common.by import By

def login(wb,uname,pwd):
    iframe = wb.find_element(By.ID, 'login_frame')
    wb.switch_to.frame(iframe)
    unam = wb.find_element(By.ID, 'u')
    unam.clear()
    unam.send_keys(uname)
    upwd = wb.find_element(By.ID, 'p')
    upwd.clear()
    upwd.send_keys(pwd)
    wb.find_element(By.ID, 'login_button').click()
    sleep(2)

    errinfo = wb.find_element(By.ID, 'err_m').text
    print("失败信息是：{}".format(errinfo))