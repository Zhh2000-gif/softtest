import time

from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.edge.options import Options

option = Options()
option.add_experimental_option('excludeSwitches',['enable-automation'])
option.add_argument('--disable-blink-features=AutomationControlled')

wb = webdriver.Edge(options=option)

wb.implicitly_wait(10)
wb.get('https://kyfw.12306.cn/otn/resources/login.html')
wb.find_element('xpath','//*[@id="J-userName"]').send_keys('15555211512')
wb.find_element('xpath','//*[@id="J-password"]').send_keys('2000zhang')
wb.find_element('xpath','//*[@id="J-login"]').click()

ele = wb.find_element('xpath','//*[@id="nc_1_n1z"]')
act = ActionChains(wb)
act.click_and_hold(ele)
act.move_by_offset(380,0)
act.release()
act.perform()

time.sleep(2)
wb.find_element('xpath','//a[text()="确定"]').click()

ele = wb.find_element('xpath','//*[@id="J-chepiao"]')
act.move_to_element(ele)
act.perform()

wb.find_element('xpath','//*[@id="megamenu-3"]/div[1]/ul/li[1]/a').click()