#定义各种信息

#定义项目地址

from time import sleep
from selenium import webdriver
bd_url="http://www.baidu.com"
mail_url="https://mail.163.com/"
qq_url="https://mail.qq.com/"
uname="zhh_2000512"
pwd="2000Zhang"
qqname="1071955126@qq.com"
qqpwd="123456"

def getDriver(url):
    #实例化浏览器
    driver=webdriver.Chrome()
    # 打开页面
    driver.get(url)
    driver.implicitly_wait(10)
    #返回浏览器对象
    return driver

def printCurInfo(driver):
    cur_title = driver.title
    cur_url = driver.current_url
    cur_handle = driver.current_window_handle

    print("当前页面的标题是：%s,\n当前页面的url是：%s，\n当前页面的句柄是：%s"
          % (cur_title, cur_url, cur_handle))

def findLocalEle(driver,locat,ele):
    try:
        local = driver.find_element(locat,ele)
    except:
        print("元素定位失败：%s"%str(locat))
    else:
        print("元素定位成功")
        return local
    finally:
        print("成功与否都要执行")