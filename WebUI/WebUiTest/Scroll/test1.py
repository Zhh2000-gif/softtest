import time
from selenium import webdriver
from selenium.webdriver.common.by import By

from Scroll import config

wb = config.getDriver(config.bd_url)

#定义超链接
wb.find_element(By.XPATH,'//div[@id="u1"]/a').click()
wb.find_element(By.XPATH,'//*[@id="TANGRAM__PSP_11__submitWrapper"]/span/a[1]').click()

handles = wb.window_handles
wb.switch_to.window(handles[-1])

print(wb.title)
print(wb.current_window_handle)
print(wb.current_url)

js = "window.scrollTo(0,2500)"
wb.execute_script(js)

# wb.close()
# wb.quit()
