import yaml

def loadyaml(filepath):
    stream = open(filepath,'r')
    data = yaml.load(stream,yaml.FullLoader)
    return data