import pytest

@pytest.fixture(scope="function",autouse=True,params=['衣服','包包','鞋子'])
def login(request):
    print("登录系统")
    yield request.param
    print("退出系统")

class TestDemo1:
    def test_01(self,login):
        print("测试用例1",login)
    def test_02(self):
        print("测试用例2")

if __name__ == '__main__':
    pytest.main(['-vs','test_demo1.py'])