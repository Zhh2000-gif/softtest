import os
import pytest

if __name__ == '__main__':
    # pytest.main(['-vs','test_login.py'])
    # 生成allure测试数据
    pytest.main(['test_login.py','--alluredir','../allure-result'])
    # 将数据生成测试报告
    os.system('allure generate ../allure-result -o ../reports')