import os
import time

import pytest
from selenium import webdriver
from selenium.webdriver.edge.options import Options

from config.loadyaml import loadyaml


class Testlogin:
    def setup(self):
        option = Options()
        option.add_experimental_option('excludeSwitches', ['enable-automation'])
        option.add_argument('--disable-blink-features=AutomationControlled')
        self.wb = webdriver.Edge(options=option)
        self.wb.get('https://leetcode.cn/')
        self.wb.implicitly_wait(10)

    def teardown(self):
        time.sleep(3)
        self.wb.quit()

    @pytest.mark.parametrize('udata',loadyaml('../data/login.yaml'))
    def test_login(self,udata):
        self.wb.find_element('xpath', '//*[@data-cypress="sign-in-with-password"]').click()
        self.wb.find_element('xpath','//*[@name="login"]').send_keys(udata['uname'])
        self.wb.find_element('xpath','//*[@name="password"]').send_keys(udata['upas'])
        self.wb.find_element('xpath','//*[@class="css-4a562t-BaseCheckbox-w16 e18ulegg0"]').click()
        self.wb.find_element('xpath','//*[@type="submit"]').click()
    def test_01(self):
        assert 1==2