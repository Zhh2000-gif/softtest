from appium.webdriver.webdriver import WebDriver
class BasePage:
    def __init__(self,driver:WebDriver=None):
        self._driver=driver
    def find(self,locator,value:str=None):
        if isinstance(locator,tuple):
            return self._driver.find_element(*locator)
        else:
            return self._driver.find_element(locator,value)
    def drop(self,stx,sty,endx,endy,time):
        self._driver.swipe(stx,sty,endx,endy,time)
