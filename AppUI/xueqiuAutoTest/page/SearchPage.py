from page.BasePage import BasePage
from appium.webdriver.common.mobileby import MobileBy
import time
class Search(BasePage):
    def searchInput(self,key):
        self.find(MobileBy.ID,"search_input_text").send_keys(key)
        self.find(MobileBy.ID,"name").click()
        time.sleep(2)
        return self

    def get_price(self):
        return float(self.find(MobileBy.ID,"current_price").text)

    def dropScreen(self):
        self.drop(185.6,665.5,176.8,249.8,4)