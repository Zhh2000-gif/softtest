from appium import webdriver
from page.main import Main
from page.BasePage import BasePage

class APP(BasePage):
    _appPackage = 'com.xueqiu.android'
    _appWaitActivity = '.view.WelcomeActivityAlias'
    def start(self):
        if self._driver is None:
            des_caps = {}
            des_caps['platformName'] = 'Android'
            des_caps['platformVersion'] = '7.1.2'
            des_caps['deviceName'] = '127.0.0.1:62001'
            des_caps['app'] = 'D:\\PythonProjects\\gitProjects\\softtest\\AppUI\\xueqiu.apk'
            des_caps['noReset'] = True
            des_caps['appPackage'] = self._appPackage
            des_caps['appWaitActivity'] = self._appWaitActivity
            des_caps['unicodeKeyboard'] = True
            des_caps['resetKeyboard'] = True
            des_caps['chromedriverExecutable'] = 'D:\\PythonProjects\\gitProjects\\softtest\\AppUI\\chromedriver\\chromedriver.exe'
            self._driver=webdriver.Remote("http://127.0.0.1:4723/wd/hub",des_caps)
            self._driver.implicitly_wait(20)
        else:
            self._driver.start_activity(self._appPackage,self._appWaitActivity)
        return self
    def main(self):
        return Main(self._driver)