import pytest
import allure

from common.readyaml import readdata
from page.app import APP
@allure.feature("搜索模块")
class TestSearch:
    @pytest.mark.parametrize("key,price",readdata("search.yml"))
    @allure.story("搜索公司股价")
    def test_search(self,key,price):
        with allure.step("1.首页搜索框，点击输入搜索的内容"):
            assert APP().start().main().goto_search_page().searchInput(key).get_price()>float(price)
    @allure.story("点击个人主页")
    def test_photo(self):
        assert APP().start().main().goto_my_page()
