from appium.webdriver import webdriver
from appium.webdriver.common.touch_action import TouchAction
appPackage = 'com.xueqiu.android'
appWaitActivity = '.view.WelcomeActivityAlias'

des_caps = {}
des_caps['platformName'] = 'Android'
des_caps['platformVersion'] = '7.1.2'
des_caps['deviceName'] = '127.0.0.1:62001'
des_caps['app'] = 'D:\\PythonProjects\\gitProjects\\softtest\\AppUI\\xueqiu.apk'
des_caps['noReset'] = True
des_caps['appPackage'] = appPackage
des_caps['appWaitActivity'] = appWaitActivity
des_caps['unicodeKeyboard'] = True
des_caps['resetKeyboard'] = True
des_caps['chromedriverExecutable'] = 'D:\\PythonProjects\\gitProjects\\softtest\\AppUI\\chromedriver\\chromedriver.exe'
driver = webdriver.Remote("http://127.0.0.1:4723/wd/hub",des_caps)
driver.implicitly_wait(20)
driver.remove_app(appPackage)


driver.start_activity(appPackage,appWaitActivity)
driver.swipe()
driver.tap([(100,200)])

TouchAction(driver).press(100,200).perform()
TouchAction(driver).wait(2000)

driver.shake()
TouchAction(driver).long_press().perform().release()
