from AppiumTestCase import param
from appium import webdriver
from appium.webdriver.common.mobileby import MobileBy
des_caps={}
des_caps['platformName']=param.PlatformName
des_caps['platformVersion']=param.PlatformVersion
des_caps['deviceName']=param.DeviceName
des_caps['app']=param.App
des_caps['noReset']=param.NoReset
des_caps['appPackage']=param.AppPackage
des_caps['appWaitActivity']=param.AppWaitActivity
des_caps['unicodeKeyboard']=param.UnicodeKeyboard
des_caps['resetKeyboard']=param.ResetKeyboard
des_caps['chromedriverExecutable']=param.ChromedriverExecutable
# desired_caps = {
#    "platformName": "Android",
#    "appium:platformVersion": "7.1.2",
#    "appium:deviceName": "127.0.0.1:62001",
#    "appium:app":"D:\\PythonProjects\\gitProjects\\softtest\\AppUI\\xueqiu.apk",
#    "appium:noReset": True,
#    "appium:appPackage": "com.xueqiu.android",
#    "appium:appWaitActivity": ".view.WelcomeActivityAlias",
#    "appium:unicodeKeyboard": True,
#    "appium:resetKeyboard": True
# }
driver=webdriver.Remote('http://127.0.0.1:4723/wd/hub',des_caps)
driver.implicitly_wait(10)

# ele=driver.find_element(MobileBy.ID,"home_search")
# ele.click()
code = 'new UiSelector().text("交易")'
ele=driver.find_element(MobileBy.ANDROID_UIAUTOMATOR,code)
ele.click()